module Logger (configLogger) where

import System.IO
import System.Log.Logger
import System.Log.Handler (setFormatter)
import System.Log.Handler.Simple
import System.Log.Formatter

configLogger :: IO ()
configLogger = do
    h <- verboseStreamHandler stderr DEBUG
    let f = simpleLogFormatter "$utcTime $prio $loggername: $msg"
    let h' = setFormatter h f
    updateGlobalLogger rootLoggerName (setLevel DEBUG)
    updateGlobalLogger rootLoggerName (setHandlers [h'])
