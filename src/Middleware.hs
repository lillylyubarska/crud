{-# LANGUAGE OverloadedStrings #-}

module Middleware where
import System.Log.Logger
import Network.Wai

comp = "Middleware.hs"


withLogging :: Application -> Application
withLogging app req respond =
  app req $ \response -> do
    debugM comp $ (show $ httpVersion req) ++
          " " ++ (show $ requestMethod req) ++
          " " ++ (show $ requestHeaders req) ++
          " " ++ (show $ responseStatus response) ++
          "\n"
    respond response
