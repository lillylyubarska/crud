{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module Config where

import System.Log.Logger
import Data.Yaml
import Data.Char (toLower)
import GHC.Generics
import Data.Aeson
import Data.Aeson.TH
import Control.Monad.IO.Class (liftIO)
import Control.Exception

comp = "Config.hs"

data Db = Db { _name :: String, _host :: String, _delay :: Int } deriving (Generic)

data Config = Config { _port :: Int,
                       _db :: Db } deriving (Generic)

concat <$> mapM (deriveJSON defaultOptions { fieldLabelModifier = map toLower . drop 1 }) [''Db, ''Config]

type ParseIO = IO (Either ParseException Config)

defaultConfig = Config { _db = Db { _name = "test", _host = "127.0.0.1", _delay = 3600000 } , _port = 3000 }

getConfig :: IO Config
getConfig = do
    result <- liftIO $ decodeFileEither "config.yml" :: ParseIO
    case result of
          Left e -> do
               debugM comp ("Cannot read config " ++ show e)
               return defaultConfig
          Right config -> do
                debugM comp "Read config successfully"
                return config
