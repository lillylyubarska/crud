{-# LANGUAGE OverloadedStrings #-}
module Db where

import Data.Text (pack)
import System.Log.Logger
import Control.Concurrent
import Control.Exception
import Database.MongoDB
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class (liftIO)
import Utils hiding (comp)
import Config hiding (comp)
import Control.Monad.Trans.Reader

comp = "Db.hs"

connectMongo :: Config -> IO Pipe
connectMongo config@(Config _ (Db _ dbHost delay)) = do
    res <- try $ connect (host dbHost) :: IO (Either IOException Pipe)
    case res of
      Left _ -> do
              debugM comp "Reconnect to db"
              threadDelay delay
              connectMongo config
      Right pipe -> do
              debugM comp "Connected to db"
              return pipe


disconnectMongo :: EnvT IO ()
disconnectMongo = do
    (_, pipe) <- ask
    liftIO $ debugM comp "Disconnect from db"
    liftIO $ close pipe

getUsers :: EnvT IO [Document]
getUsers = do
    (Config _ (Db name _ _), pipe) <- ask
    access pipe master (pack name) findUsers where
            findUsers = rest =<< find (select [] "users")

