{-# LANGUAGE OverloadedStrings #-}

module Handlers  where

import System.Log.Logger
import Web.Scotty
import Web.Scotty.Internal.Types
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class (liftIO)
import Utils hiding (comp)
import Db (getUsers)
import Control.Monad.Trans.Class (lift)
import Control.Concurrent.Async
import Data.AesonBson

comp = "Handlers.hs"

homePageHandler :: EnvT ActionM ()
homePageHandler = do
      lift $ file "./static/public/index.html"

getUsersHandler :: EnvT ActionM ()
getUsersHandler = do
    env <- ask
    asyncUsers <- liftIO $ async (runReaderT getUsers env)
    users <- lift $ liftAndCatchIO $ wait asyncUsers
    lift $ json $ map aesonify users
