module Utils  where

import Control.Monad.Trans.Reader
import Database.MongoDB (Pipe)
import Config
import Data.Text.Internal.Lazy
import Web.Scotty
import Network.HTTP.Types.Status

comp = "Utils.hs"

type Env = (Config, Pipe)
type EnvT = ReaderT Env

handleError :: Text -> ActionM ()
handleError msg = status internalServerError500 >> text msg
