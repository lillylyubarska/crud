{-# LANGUAGE OverloadedStrings #-}
module Main where

import Web.Scotty
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class (liftIO)
import Network.Wai
import Network.Wai.Middleware.Static
import System.Log.Logger
import Handlers hiding (comp)
import Db (connectMongo, disconnectMongo)
import Utils hiding (comp)
import Config hiding (comp)
import Middleware (withLogging)
import Logger (configLogger)
import Network.HTTP.Types.Status

comp = "Main.hs"

routine :: EnvT IO ()
routine = do
      env@(Config port _, pipe) <- ask
      let app = do
              middleware $ staticPolicy (noDots >-> addBase "static/public")
              middleware withLogging
              get "/" $ do
                  runReaderT homePageHandler env `rescue` handleError
              get "/api/users" $ do
                  runReaderT getUsersHandler env `rescue` handleError
              notFound $ text "Not found" >> status notFound404

      liftIO $ scotty port app
      disconnectMongo

main = do
    configLogger
    debugM comp "ooo000000 Start 0000000ooo"
    config <- getConfig
    pipe <- connectMongo config
    runReaderT routine (config, pipe)

